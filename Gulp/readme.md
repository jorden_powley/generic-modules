# Gulp
Basic Gulp installation example.
Runs basic minification and uglify on SCSS and JS files

## Installation
Copy `package.json` over to the required project folder.
If not a fresh NPM install then ensure that the dev-dependences are copied over.
Copy `gulpfile.js` over to the project root. 
# Secure Trading Gateway
A simplified codeset on how to deploy two types of Secure Trading Payment Pages integration.
One integration use site security to prevent down-grade and header based attacks.
The site security integration (recommended) is built to try and mitigate this attack.

## Setup
Requires a config.json file to be created. Use the config-example.json to help.
Will need a Secure Trading account.

## Disclaimer
It should be noted that Secure Trading will keep firing redirect requests until it recieves a confirmation. To stop multiple transactions from being posted; a storage mechanism is required where a GUID can only be used once.

## Return Statuses
 * 0 - Pending settlement
 * 1 - Pending settlement, bypassing fraud and duplicate checks
 * 10 - Settlement process underway
 * 100 - Settled
 * 2 - Transaction suspended
 * 3 - Transaction cancelled

## Test Cards
<https://custom-gateway.zendesk.com/hc/en-us/articles/205024168-Secure-Trading-test-cards>
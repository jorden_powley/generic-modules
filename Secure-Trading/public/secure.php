<?php

/**
 * ------------------------------------------------
 * Secure Trading Standard Payment Page
 * ------------------------------------------------
 * @desc - Standard payment page demonstration
 * ------------------------------------------------
 */

// Pull config
$config;
if(!file_exists("../config.json")){
    die("No configuration file");
} else{
    $config = json_decode(file_get_contents("../config.json"), true);
}

// Prepare Secure Trading Values
$reference = md5(date("Ymd_His"));

$secureTrading = array(
    "order_reference" => "Gateway Test " . $reference,
    "st_currency_iso" => $config['SECURE_TRADING']['ST_CURRECY_ISO'],
    "st_site_reference" => $config['SECURE_TRADING']['ST_SITE_REFERENCE'],
    "successful_redirect" => $config['BASE_URL'] . "/standard-complete.php?transaction=success",
    "failure_redirect" => $config['BASE_URL'] . "/standard-complete.php?transaction=failed",
    "st_version" => $config['SECURE_TRADING']['ST_VERSION'],
    "st_profile" => $config['SECURE_TRADING']['ST_PROFILE'],
    "st_default_profile" => $config['SECURE_TRADING']['ST_DEFAULT_PROFILE'],
    "st_account_description" => $config['SECURE_TRADING']['ST_ACCOUNT_TYPE']
);

?>
<?php include "partials/_head.php" ?>
<?php include "partials/_header.php" ?>
<div class="px-3 py-3 pb-md-5 mb-10 mx-auto text-center">
    <div class="lead">Coming soon</div>
</div>
<div class="container">
    <a href="/" class="btn btn-block btn-outline-primary">Back</a>
</div>
<?php include "partials/_footer.php" ?>
<?php include "partials/_foot.php" ?>
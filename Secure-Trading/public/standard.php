<?php

/**
 * ------------------------------------------------
 * Secure Trading Standard Payment Page
 * ------------------------------------------------
 * @desc - Standard payment page demonstration
 * ------------------------------------------------
 */

// Pull config
$config;
if(!file_exists("../config.json")){
    die("No configuration file");
} else{
    $config = json_decode(file_get_contents("../config.json"), true);
}

// Prepare Secure Trading Values
$reference = md5(date("Ymd_His"));

$secureTrading = array(
    "order_reference" => "Gateway Test " . $reference,
    "st_currency_iso" => $config['SECURE_TRADING']['ST_CURRECY_ISO'],
    "st_site_reference" => $config['SECURE_TRADING']['ST_SITE_REFERENCE'],
    "successful_redirect" => $config['BASE_URL'] . "/standard-complete.php?transaction=success",
    "failure_redirect" => $config['BASE_URL'] . "/standard-complete.php?transaction=failed",
    "st_version" => $config['SECURE_TRADING']['ST_VERSION'],
    "st_profile" => $config['SECURE_TRADING']['ST_PROFILE'],
    "st_default_profile" => $config['SECURE_TRADING']['ST_DEFAULT_PROFILE'],
    "st_account_description" => $config['SECURE_TRADING']['ST_ACCOUNT_TYPE']
);

?>
<?php include "partials/_head.php" ?>
<?php include "partials/_header.php" ?>
<div class="px-3 py-3 pb-md-5 mb-10 mx-auto text-center">
    <div class="lead">Please fill out the below form to demo the gateway.</div>
</div>
<div class="container" style="max-width: 600px;">
    <form method="POST" action="https://payments.securetrading.net/process/payments/details" id="secure_trading_form">
        <!-- 
            User Editable Form Fields
         -->
        <div class="form-group">
            <label for="customerfirstname">First name:</label>
            <input type="text" class="form-control" id="customerfirstname" name="customerfirstname" required>
        </div>
        <div class="form-group">
            <label for="customerlastname">Last name:</label>
            <input type="text" class="form-control" id="customerlastname" name="customerlastname" required>
        </div>
        <div class="form-group">
            <label for="customeremail">Email address:</label>
            <input type="email" class="form-control" id="customeremail" name="customeremail" required>
        </div>
        <div class="form-group">
            <label for="mainamount">Amount:</label>
            <select name="mainamount" id="mainamount" class="form-control">
                <option value="1.00">£1</option>
                <option value="5.00">£5</option>
                <option value="10.00">£10</option>
                <option value="20.00">£20</option>
            </select>
        </div>
        <!-- 
            Minimal Secure Trading Form Settings
        -->
        <input type="hidden" name="sitereference" value="<?php echo $secureTrading['st_site_reference']; ?>" />
        <input type="hidden" name="currencyiso3a" value="<?php echo $secureTrading['st_currency_iso']; ?>" />
        <input type="hidden" name="version" value="<?php echo $secureTrading['st_version']; ?>" />
        <input type="hidden" name="stprofile" value="<?php echo $secureTrading['st_profile']; ?>">
        <input type="hidden" name="stdefaultprofile" value="<?php echo $secureTrading['st_default_profile']; ?>" />
        <input type="hidden" name="accounttypedescription" value="<?php echo $secureTrading['st_account_description']; ?>" />
        <!-- 
            Redirect Rules
        -->
        <input type="hidden" name="ruleidentifier" value="STR-6" /><!-- Enable successful URL redirect -->
        <input type="hidden" name="successfulurlredirect" value="<?php echo $secureTrading['successful_redirect']; ?>" />
        <input type="hidden" name="ruleidentifier" value="STR-7" /><!-- Enable decline URL redirect -->
        <input type="hidden" name="declinedurlredirect" value="<?php echo $secureTrading['failure_redirect']; ?>" />
        <input type="hidden" name="stextraurlredirectfields" value="customerfirstname" />
        <input type="hidden" name="stextraurlredirectfields" value="customerlastname" />
        <input type="hidden" name="stextraurlredirectfields" value="customeremail" />
        <input type="hidden" name="stextraurlredirectfields" value="mainamount" />
        <!-- 
            Transaction Based Variables
        -->
        <input type="hidden" name="orderreference" value="<?php echo $secureTrading['order_reference']; ?>" />

        <button type="submit" class="btn btn-primary mt-auto">Submit</button>
    </form>
</div>
<?php include "partials/_footer.php" ?>
<?php include "partials/_foot.php" ?>
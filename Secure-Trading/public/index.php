<?php

/**
 * ------------------------------------------------
 * Secure Trading Index Page
 * ------------------------------------------------
 * @desc - Display basic navigation to go to the 
 *         two types of Secure Trading Integration
 * ------------------------------------------------
 */

?>

<?php include "partials/_head.php" ?>
<?php include "partials/_header.php" ?>
<div class="px-3 py-3 pb-md-5 mb-10 mx-auto text-center">
    <div class="lead">Please select a gateway version to demo.</div>
</div>
<div class="container" style="max-width: 600px;">
    <div class="card-deck mb-3 text-center mw-75">
        <div class="card mb-4 box-shadow">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal">Standard</h4>
            </div>
            <div class="card-body d-flex flex-column">
                <p>Standard gateway integration. Non-secure.</p>
                <a href="standard.php" class="btn btn-lg btn-block btn-outline-primary mt-auto">Standard</a>
            </div>
        </div>
        <div class="card mb-4 box-shadow">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal">Secure</h4>
            </div>
            <div class="card-body d-flex flex-column">
                <p>Secure gateway integration that requires the use of site security.</p>
                <a href="secure.php" class="btn btn-lg btn-block btn-secondary mt-auto">Secure</a>
            </div>
        </div>
    </div>
</div>
<?php include "partials/_footer.php" ?>
<?php include "partials/_foot.php" ?>
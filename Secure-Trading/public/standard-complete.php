<?php

/**
 * ------------------------------------------------
 * Secure Trading Standard Completion Page
 * ------------------------------------------------
 * @desc - Standard payment page redirection
 *         demonstration.
 * ------------------------------------------------
 */

// Check GET params have been passed
if(!isset($_GET) || empty($_GET)){
    header("Location: /standard.php");
}

// Pull out the required values from the redirect
$_GET['transaction'] = (array_key_exists('transaction', $_GET) && isset($_GET['transaction']) && !empty($_GET['transaction'])) ? $_GET['transaction'] : "";
$_GET['customeremail'] = (array_key_exists('customeremail', $_GET) && isset($_GET['customeremail']) && !empty($_GET['customeremail'])) ? $_GET['customeremail'] : "";
$_GET['customerfirstname'] = (array_key_exists('customerfirstname', $_GET) && isset($_GET['customerfirstname']) && !empty($_GET['customerfirstname'])) ? $_GET['customerfirstname'] : "";
$_GET['customerlastname'] = (array_key_exists('customerlastname', $_GET) && isset($_GET['customerlastname']) && !empty($_GET['customerlastname'])) ? $_GET['customerlastname'] : "";
$_GET['mainamount'] = (array_key_exists('mainamount', $_GET) && isset($_GET['mainamount']) && !empty($_GET['mainamount'])) ? $_GET['mainamount'] : "";
$_GET['orderreference'] = (array_key_exists('orderreference', $_GET) && isset($_GET['orderreference']) && !empty($_GET['orderreference'])) ? $_GET['orderreference'] : "";
$_GET['transactionreference'] = (array_key_exists('transactionreference', $_GET) && isset($_GET['transactionreference']) && !empty($_GET['transactionreference'])) ? $_GET['transactionreference'] : "";
$_GET['settlestatus'] = (array_key_exists('settlestatus', $_GET) && isset($_GET['settlestatus']) && !empty($_GET['settlestatus']) || (intval($_GET['settlestatus']) === 0)) ? $_GET['settlestatus'] : "";

?>
<?php include "partials/_head.php" ?>
<?php include "partials/_header.php" ?>
<div class="px-3 py-3 pb-md-5 mb-10 mx-auto text-center">
    <div class="lead">Please check your order summary below.</div>
</div>
<div class="container">
    <table class="table">
        <tr>
            <th>Transaction status:</th>
            <td><?php echo $_GET['transaction']; ?></td>
        </tr>
        <tr>
            <th>Customer email:</th>
            <td><?php echo $_GET['customeremail']; ?></td>
        </tr>
        <tr>
            <th>Customer first name:</th>
            <td><?php echo $_GET['customerfirstname']; ?></td>
        </tr>
        <tr>
            <th>Customer last name:</th>
            <td><?php echo $_GET['customerlastname']; ?></td>
        </tr>
        <tr>
            <th>Value:</th>
            <td>£<?php echo $_GET['mainamount']; ?></td>
        </tr>
        <tr>
            <th>Order reference:</th>
            <td><?php echo $_GET['orderreference']; ?></td>
        </tr>
        <tr>
            <th>Transaction reference</th>
            <td><?php echo $_GET['transactionreference']; ?></td>
        </tr>
        <tr>
            <th>Settle status</th>
            <td><?php echo $_GET['settlestatus']; ?></td>
        </tr>
    </table>
    <div class="alert alert-warning">
        <p>It should be noted that Secure Trading will keep firing redirect requests until it recieves a confirmation. To stop multiple transactions from being posted; a storage mechanism is required where a GUID can only be used once.</p>
        <p>Details above are only provided as an example of some of the data that could be pulled back from the gateway.</p>
        <p>An example of settle statuses:</p>
        <ul>
            <li><strong>0</strong> - Pending settlement</li>
            <li><strong>1</strong> - Pending settlement, bypassing fraud and duplicate checks</li>
            <li><strong>10</strong> - Settlement process underway</li>
            <li><strong>100</strong> - Settled</li>
            <li><strong>2</strong> - Transaction suspended</li>
            <li><strong>3</strong> - Transaction cancelled</li>
        </ul>
    </div>
    <div class="container">
        <a href="standard.php" class="btn btn-block btn-outline-primary">Restart</a>
    </div>
</div>
<?php include "partials/_footer.php" ?>
<?php include "partials/_foot.php" ?>
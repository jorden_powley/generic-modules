# Generic-Modules
This contains / will contain various 'mini' projects of reusable code.

## Validations
Contains various different validation functions / methods in various different languages.
[PHP, jQuery]

## Notifications
Currently a basic jQuery notification library

## Worldpay
Contains a quick, easy to use code set for Worldpay payment integrations

## Secure-Trading
In development
/**
 * ----------------------------------------------------------------
 * JS Validation Functions
 * ----------------------------------------------------------------
 * @author - Jorden Powley <jorden.powley.webdev@gmail.com>
 * @requires - jQuery
 */

// ----------------------------------------------------------------
// ------------------------Global Variables------------------------
// ----------------------------------------------------------------
var validationDebug = false;


/**
 * setValidationDebug()
 * @desc - Sets the debug function on
 * @param {boolean} value       - Debug status to be set
 */
function setValidationDebug(value){
    if(value === true){
        validationDebug = true;
    } else{
        validationDebug = false;
    }
}

/**
 * printValidationDebug()
 * @desc - Prints out debugging messages
 * @param {String} message      - Message to be printed
 */
function printValidationDebug(message){
    // Check debug status and print
    if(validationDebug === true){
        console.log(message);
    }
}

/**
 * validateSet()
 * @desc -  Checks that the value is set (Does not equal NULL or UNDEFINED)
 * @param {String} value        - Value to be checked
 * @return {boolean}
 */
function validateSet(value){
    // Check value
    if((value !== null) && (value !== undefined)){
        printValidationDebug("Value {" + value + "} is not null or undefined");
        return true;
    } else{
        printValidationDebug("Value {" + value + "} is null or undefined");
        return false;
    }
}

/**
 * validateNotEmpty()
 * @desc - Checks that the value is set and not empty
 * @param {String} value        - Value to be validated
 * @return {boolean}
 */
function validateNotEmpty(value){
    // Validate as set 
    if(validateSet(value)){
        if((value === true) || (value === false)){
            // Value is set as boolean
            printValidationDebug("Value {" + value + "} is set as a boolean");
            return true;
        } else if($.isNumeric(value)){
            // Value is numeric
            printValidationDebug("Value {" + value + "} is numeric");
            return true;
        } else if(value.length >= 1){
            // Value is a string of a length greater or equal to 1
            printValidationDebug("Value {" + value + "} is a string of a length greater or equal to 1");
            return true;
        } else{
            // Check debug status and print
            printValidationDebug("Value {" + value + "} is empty");
            return false;
        }
    } else{
        // Value is not set
        printValidationDebug("Value {" + value + "} is null or undefined");
        return false;
    }
}

/**
 * validateNumeric()
 * @desc - Checks that the value is numeric
 * @param {int} value       - The value to be checked
 * @return {boolean}
 */
function validateNumeric(value){
    // Trim the value
    value = $.trim(value);

    // Check value is set
    var checked = validateSet(value);
    if(checked === true){
        // Value is set, check that it is empty
        if($.isNumeric(value)){
            // Value is numeric 
            printValidationDebug("Value {" + value + "} is numeric");
            return true;
        } else{
            printValidationDebug("Value {" + value + "} is not numeric");
            return false;
        }
    } else{
        printValidationDebug("Value {" + value + "} is not set");
        return false;
    }
}

/**
 * validateEmail()
 * @desc - Validate an email address
 * @param {String} value        - Value to be validated
 * @return {boolean}
 */
function validateEmail(value){
    // Trim the value
    value = $.trim(value);

    var filter = /[A-Z0-9a-z_%+-]+[A-Z0-9a-z._%+-]+\@[A-Z0-9a-z._%+-]+\.[A-Z0-9a-z._%+-]+/;
    // Check length
    if(value.length > 4){
        if(filter.test(value)){
            // Email is valid
            printValidationDebug("Value {" + value + "} is a valid email");
            return true;
        } else{
            // Email does not match the regex
            printValidationDebug("Value {" + value + "} does not match the regex");
            return false;
        }
    } else{
        printValidationDebug("Value {" + value + "} is not long enough to be an email");
        return false;
    }
}

/**
 * validateHTTP()
 * @desc - Validate an HTTP/HTTPS Link
 * @param {String} value        - Value to be validated
 * @return {boolean} 
 */
function validateHTTP(value){
    // Trim the value
    value = $.trim(value);

    // Determine whether secure
    if(value.startsWith("https://")){
        // Location valid https
        printValidationDebug("Value {" + value + "} is valid https");
        return true;
    } else if(value.startsWith("http://")){
        // Location valid http
        printValidationDebug("Value {" + value + "} is valid http");
        return true;
    } else{
        // Location not valid
        printValidationDebug("Value {" + value + "} is not a valid location");
        return false;
    }
}

/**
 * validateHTTPS()
 * @desc - Validate an HTTPS Link
 * @param {String} value        - Value to be validated
 * @return {boolean} 
 */
function validateHTTPS(value){
    // Trim the value
    value = $.trim(value);

    // Determine whether secure
    if(value.startsWith("https://")){
        // Location valid https
        printValidationDebug("Value {" + value + "} is valid https");
        return true;
    } else{
        // Location not valid
        printValidationDebug("Value {" + value + "} is not a valid location");
        return false;
    }
}

/**
 * validatePasswordStrength()
 * @desc - Validates the strength of a password
 * @param {String} value        - Value to be validated
 * @param {int} level           - Password strength required, Defaults to 0
 *                              - [0] alphanumeric minimum
 *                              - [1] alphanumeric, uppercase minimum
 *                              - [2] alphanumeric, uppercase, symbols minimum 
 * @param {int} length          - Length of password characters, Defaults to 4
 * @return {boolean}
 */
function validatePasswordStrength(value, level = 0, length = 4) {
    // Trim the value
    value = $.trim(value);

    // REGEXs
    var anyCaseRegex = /[a-zA-Z]/g;
    var lowerCaseRegex = /[a-z]/g;
    var upperCaseRegex = /[A-Z]/g;
    var numericRegex = /[0-9]/g;
    var symbolRegex = /[$&+,:;=?@#|'<>.\-^*()%!]/g;

    // Check length
    if (value.length >= length) {
        // Check for lowercase letter
        if (anyCaseRegex.exec(value) !== null) {
            // Check for numeric
            if (numericRegex.exec(value) !== null) {
                // See if stronger is required
                if (level >= 1) {
                    // Check for uppercase letter
                    if (upperCaseRegex.exec(value) !== null) {
                        // Check for lowercase letter
                        if (lowerCaseRegex.exec(value) !== null) {
                            // See if stronger is required
                            if (level >= 2) {
                                // Check for symbols
                                if (symbolRegex.exec(value) !== null) {
                                    // Password is strong enough
                                    printValidationDebug("Value {" + value + "} is strong enough [L-2].");
                                    return true;
                                } else {
                                    // Password does not contain a symbol
                                    printValidationDebug("Value {" + value + "} does not contain a symbol");
                                    return false;
                                }
                            } else {
                                // Required strength met -> Return
                                printValidationDebug("Value {" + value + "} is strong enough [L-1].");
                                return true;
                            }
                        } else {
                            // Password does not contain an lowercase letter
                            printValidationDebug("Value {" + value + "} does not contain an lowercase letter");
                            return false;
                        }
                    } else {
                        // Password does not contain an uppercase letter
                        printValidationDebug("Value {" + value + "} does not contain an uppercase letter");
                        return false;
                    }
                } else {
                    // Required strength met -> Return
                    printValidationDebug("Value {" + value + "} is strong enough [L-0].");
                    return true;
                }
            } else {
                // Password does not contain a number
                printValidationDebug("Value {" + value + "} does not contain a number");
                return false;
            }
        } else {
            // Password does not contain a letter
            printValidationDebug("Value {" + value + "} does not contain a letter");
            return false;
        }
    } else {
        // Password not long enough
        printValidationDebug("Value {" + value + "} is not a long enough password");
        return false;
    }
}

/**
 * validateCSSColor()
 * @desc - Validates a user input string to be a valid CSS color
 * @param {String} value        - String to be validated
 * @return {boolean}
 */
function validateCSSColor(value){
    // Function variables
    var wordedColors = ["aliceblue", "antiquewhite", "aqua", "aquamarine", "azure", "beige", "bisque", "black", "blanchedalmond", "blue", "blueviolet", "brown", "burlywood", "cadetblue", "chartreuse", "chocolate", "coral", "cornflowerblue", "cornsilk", "crimson", "cyan", "darkblue", "darkcyan", "darkgoldenrod", "darkgray", "darkgreen", "darkkhaki", "darkmagenta", "darkolivegreen", "darkorange", "darkorchid", "darkred", "darksalmon", "darkseagreen", "darkslateblue", "darkslategray", "darkturquoise", "darkviolet", "deeppink", "deepskyblue", "dimgray", "dodgerblue", "firebrick", "floralwhite", "forestgreen", "fuchsia", "gainsboro", "ghostwhite", "gold", "goldenrod", "gray", "green", "greenyellow", "honeydew", "hotpink", "indianred", "indigo", "ivory", "khaki", "lavender", "lavenderblush", "lawngreen", "lemonchiffon", "lightblue", "lightcoral", "lightcyan", "lightgoldenrodyellow", "lightgray", "lightgreen", "lightpink", "lightsalmon", "lightseagreen", "lightskyblue", "lightslategray", "lightsteelblue", "lightyellow", "lime", "limegreen", "linen", "magenta", "maroon", "mediumaquamarine", "mediumblue", "mediumorchid", "mediumpurple", "mediumseagreen", "mediumslateblue", "mediumspringgreen", "mediumturquoise", "mediumvioletred", "midnightblue", "mintcream", "mistyrose", "moccasin", "navajowhite", "navy", "oldlace", "olive", "olivedrab", "orange", "orangered", "orchid", "palegoldenrod", "palegreen", "paleturquoise", "palevioletred", "papayawhip", "peachpuff", "peru", "pink", "plum", "powderblue", "purple", "rebeccapurple", "red", "rosybrown", "royalblue", "saddlebrown", "salmon", "sandybrown", "seagreen", "seashell", "sienna", "silver", "skyblue", "slateblue", "slategray", "snow", "springgreen", "steelblue", "tan", "teal", "thistle", "tomato", "turquoise", "violet", "wheat", "white", "whitesmoke", "yellow", "yellowgreen"];
    var hexREGEX = /^#[0-9A-Fa-f]{6}$|^#[0-9A-Fa-f]{3}$/i;
    var rgbaREGEX = /rgba\((\d{1}|\d{2}|\d{3}),(\d{1}|\d{2}|\d{3}),(\d{1}|\d{2}|\d{3}),(\d{1}|\d{1}.\d{1}|\d{1}.\d{2})\)/g;
    var rgbREGEX = /rgb\((\d{1}|\d{2}|\d{3}),(\d{1}|\d{2}|\d{3}),(\d{1}|\d{2}|\d{3})\)/g;

    // Remove whitespace from the value and convert to lowercase
    value = value.replace(/\s/g, '');
    value = value.toLowerCase();

    // Check first characters and split validation dependent
    if(value.startsWith("#")){
        // String is a hex code
        // Check the string length is 4 OR 7 only
        if((value.length === 4) || (value.length === 7)){
            // Check correct characters 
            if(hexREGEX.test(value)){
                printValidationDebug("Value {" + value + "} is a valid HEX color");
                return true;
            } else{
                // Hex uses invalid characters
                printValidationDebug("Value {" + value + "} HEX uses invalid characters");
                return false;
            }
        } else{
            // Hex is not of valid length
            printValidationDebug("Value {" + value + "} HEX is not of a valid length");
            return false;
        }
    } else if(value.startsWith("rgba(")){
        // String is a rgba
        // Check the RGB string has the correct character structure
        if(rgbaREGEX.test(value)){
            // Remove un-needed characters
            value = value.replace("rgba(", "");
            value = value.replace(")", "");

            // Breakout number values
            var matches = value.split(",");

            // Check each match is between 0 - 255 and 0 - 1
            // Check first number
            if((parseInt(matches[0]) >= 0) && (parseInt(matches[0]) <= 255)){
                // Check second number
                if((parseInt(matches[1]) >= 0) && (parseInt(matches[1]) <= 255)){
                    // Check third number
                    if((parseInt(matches[2]) >= 0) && (parseInt(matches[2]) <= 255)){
                        // Check the fourth number
                        if((parseInt(matches[3]) >= 0) && (parseInt(matches[3]) <= 1)){
                            // Value is a valid RGBA color
                            printValidationDebug("Value {" + value + "} is a valid RGBA color");
                            return true;
                        } else{
                            // RGBA fourth number is not in range
                            printValidationDebug("Value {" + value + "} RGBA fourth number not in range");
                            return false;
                        }
                    } else{
                        // RGBA third number is not in range
                        printValidationDebug("Value {" + value + "} RGBA third not in range");
                        return false;
                    }
                } else{
                    // RGBA second number is not in range
                    printValidationDebug("Value {" + value + "} RGBA second number is not in range");
                    return false;
                }
            } else{
                // RGBA first number is not in range
                printValidationDebug("Value {" + value + "} RGBA first number is not in range");
                return false;
            }
        } else{
            // RGBA string does not have the correct structure
            printValidationDebug("Value {" + value + "} RGBA does not have the correct structure");
            return false;
        }
    } else if(value.startsWith("rgb(")){
        // String is a rgb
        // Check the RGB string has the correct character structure
        if(rgbREGEX.test(value)){
            // Remove un-needed characters
            value = value.replace("rgb(", "");
            value = value.replace(")", "");

            // Breakout number values
            var matches = value.split(",");

            // Check each match is between 0 - 255
            // Check first number
            if((parseInt(matches[0]) >= 0) && (parseInt(matches[0]) <= 255)){
                // Check second number
                if((parseInt(matches[1]) >= 0) && (parseInt(matches[1]) <= 255)){
                    // Check third number
                    if((parseInt(matches[2]) >= 0) && (parseInt(matches[2]) <= 255)){
                        // Value is valid RGB CSS
                        printValidationDebug("Value {" + value + "} is a valid RGB color");
                        return true;
                    } else{
                        // RGB third number is not in range
                        printValidationDebug("Value {" + value + "} RGB third number is not in range");
                        return false;
                    }
                } else{
                    // RGB second number is not in range
                    printValidationDebug("Value {" + value + "} RGB second number is not in range");
                    return false;
                }
            } else{
                // RGB first number is not in range
                printValidationDebug("Value {" + value + "} RGB first number is not in range");
                return false;
            }
        } else{
            // RGB string does not have the correct structure
            printValidationDebug("Value {" + value + "} RGB does not have the correct structure");
            return false;
        }
    } else if($.inArray(value, wordedColors) !== -1){
        // String is a worded color
        printValidationDebug("Value {" + value + "} is a worded color");
        return true;
    } else{
        // String is not valid
        printValidationDebug("Value {" + value + "} is not valid");
        return false;
    }
}

/**
 * validateBoolean()
 * @desc - Validates a value as boolean
 * @param {boolean} value       - value to be validated
 * @return {boolean}
 */
function validateBoolean(value){
    // Check value is set 
    if((value === true) || (value === false)){
        // Value is a direct boolean
        printValidationDebug("Value {" + value + "} is a valid boolean");
        return true;
    } else{
        // Trim the value
        value = $.trim(value);

        if((value === "true") || (value === "false")){
            // Value is a valid string boolean
            printValidationDebug("Value {" + value + "} is a valid string boolean");
            return true;
        } else{
            if ((value === 1) || (value === 0) || (value === "1") || (value === "0")) {
                // Value is a valid numeric boolean
                printValidationDebug("Value {" + value + "} is a valid boolean");
                return true;
            } else {
                // value is not a numeric boolean
                printValidationDebug("Value {" + value + "} is not a valid numeric boolean");
                return false;
            }
        }
    }
}

/**
 * validateInArray()
 * @desc - Validates a value to be in array
 * @param {Unknown} value       - Value to be validated
 * @param {Object[]} array      - Array to be checked against
 * @return {boolean}
 */
function validateInArray(value, array){
    // Trim the value
    value = $.trim(value);
    
    // Check value is set
    if(validateSet(value)){
        if($.inArray(value, array) !== -1){
            // Value is in array
            printValidationDebug("Value {" + value + "} is in array");
            return true;
        } else{
            // Value is not in array
            printValidationDebug("Value {" + value + "} is not in array");
            return false;
        }
    } else{
        // Value is not set
        printValidationDebug("Value {" + value + "} is not set");
        return false;
    }
}

/**
 * validateTitle()
 * @desc - Validates a title
 * @param {String} value    - Title to be validated
 * @return {boolean}
 */
function validateTitle(value){
    var validTitles = ["MR", "MRS", "MISS", "MASTER", "MS", "MX", "MADAM", "DR", "MADDAM"];

    // Trim the value
    value = $.trim(value);

    // Check input is set and not empty
    if(validateSet(value) && validateNotEmpty(value)){
        if(validateInArray(value, validTitles)){
            // Value is a title
            printValidationDebug("Value {" + value + "} is a valid title");
            return true;
        } else{
            // Value is not in array
            printValidationDebug("Value {" + value + "} is not in title array");
            return false;
        }
    } else{
        // Value is not set or is empty
        printValidationDebug("Value {" + value + "} is not set or empty");
        return false;
    }
}

/**
 * validateAlphaNumeric()
 * @desc - Non-Strict version of alphanumeric validation
                                => lowercase alphabet
                                => uppercase alphabet
                                => numbers
                                => whitespace
                                => symbols [-] [_]
 * @param {String} value        - Value to be validated
 * @return {boolean}
 */
function validateAlphaNumeric(value){
    // Trim the value
    value = $.trim(value);

    // Check input is set and not empty
    if(validateSet(value) && validateNotEmpty(value)){
        // Check value only contains letters numbers and spaces
        if(value.match(/^[a-zA-Z0-9\s\-\_]+$/)){
            // String is valid
            printValidationDebug("Value {" + value + "} is a valid AlphaNumeric");
            return true;
        } else{
            printValidationDebug("Value {" + value + "} contains invalid characters");
            return false;
        }
    } else{
        // Value is not set or is empty
        printValidationDebug("Value {" + value + "} is not set or empty");
        return false;
    }
}

/**
 * validateAlphaNumericStrict()
 * @desc - Strict version of alphanumeric validation
                                => lowercase alphabet
                                => uppercase alphabet
                                => numbers
                                => symbols [-] [_]
 * @param {String} value        - Value to be validated
 * @return {boolean}
 */
function validateAlphaNumericStrict(value) {
    // Trim the value
    value = $.trim(value);

    // Check input is set and not empty
    if (validateSet(value) && validateNotEmpty(value)) {
        // Check value only contains letters numbers and spaces
        if (value.match(/^[a-zA-Z0-9\-\_]+$/)) {
            // String is valid
            printValidationDebug("Value {" + value + "} is a valid Strict AlphaNumeric");
            return true;
        } else {
            printValidationDebug("Value {" + value + "} contains invalid characters");
            return false;
        }
    } else {
        // Value is not set or is empty
        printValidationDebug("Value {" + value + "} is not set or empty");
        return false;
    }
}

/**
 * validateText()
 * @desc - Validates a block of text
                                => lowercase alphabet
                                => uppercase alphabet
                                => numbers
                                => symbols [-] [_] [s] [!] ["] [£] [$] [%] [^] [&] [*] [(] [)] [{] [}] [[] []] ['] [@] [#] [~] [,] [<] [.] [>] [/] [?] [|] [\]
 * @param {String} value        - Value to be validated
 * @return {boolean}
 */
function validateText(value) {
    // Trim the value
    value = $.trim(value);    

    // Check input is set and not empty
    if (validateSet(value) && validateNotEmpty(value)) {
        // Check value only contains letters numbers and spaces
        if (value.match(/^[a-zA-Z0-9\-\_\s\!\"\£\$\%\^\&\*\(\)\{\}\[\]\'\@\#\~\,\<\.\>\/\?\\\|]+$/)) {
            // String is valid
            printValidationDebug("Value {" + value + "} is a valid block of text");
            return true;
        } else {
            printValidationDebug("Value {" + value + "} contains invalid characters");
            return false;
        }
    } else {
        // Value is not set or is empty
        printValidationDebug("Value {" + value + "} is not set or empty");
        return false;
    }
}

/**
 * validateBirthday()
 * @desc - Validates a date for birthday
 * @param {String} day     - Day to be validated
 * @param {String} month   - Month to be validated
 * @param {String} year    - Year to be validated
 * @return {boolean}
 */
function validateBirthday(day, month, year){
    var days31 = [1, 3, 5, 7, 8, 10, 12];
    var days30 = [4, 6, 9, 11];
    var startYear = 1900;
    var endYear = 3000;

    // Check inputs are set and not empty
    if((validateSet(day) && validateNotEmpty(day)) && (validateSet(month) && validateNotEmpty(month)) && (validateSet(year) && validateNotEmpty(year))) {
        // Check each input is numeric
        if($.isNumeric(day) && $.isNumeric(month) && $.isNumeric(year)){
            // Check dates fit within range
            day = parseInt(day);
            month = parseInt(month);
            year = parseInt(year);

            // Check year input
            if((year >= startYear) && (year <= endYear)){
                // Check month input 
                if((month > 0) && (month <= 12)){
                    // Check day input
                    if(validateInArray(month, days31)){
                        // Month has 31 days
                        if ((day > 0) && (day <= 31)){
                            // Day is valid
                            return true;
                        } else{
                            // Day is not valid 
                            return false;
                        }
                    } else if(validateInArray(month, days30)){
                        // Month has 30 days
                        if ((day > 0) && (day <= 30)){
                            // Day is valid
                            return true;
                        } else{
                            // Day is not valid 
                            return false;
                        }
                    } else{
                        // Month is Feb
                        if ((day > 0) && (day <= 29)){
                            // Day is valid
                            return true;
                        } else{
                            // Day is not valid 
                            return false;
                        }
                    }
                } else{
                    // Month is not valid
                    return false;
                }
            } else{
                // Year is not valid
                return false;
            }
        } else{
            // Input is not numeric
            return false;
        }
    } else{
        // Not set or empty
        return false;
    }
}

/**
 * validateHomePhone()
 * @desc - Validates a UK Home Phone Number
 * @param {String} value        - Value to be validated
 * @return {boolean}
 */
function validateHomePhone(value) {
    // Trim the value
    value = $.trim(value);

    // Check input is set and not empty
    if (validateSet(value) && validateNotEmpty(value)) {
        // Check value matches regex
        if (value.match(/^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/)) {
            // String is valid
            printValidationDebug("Value {" + value + "} is a valid home phone number");
            return true;
        } else {
            printValidationDebug("Value {" + value + "} contains invalid characters");
            return false;
        }
    } else {
        // Value is not set or is empty
        printValidationDebug("Value {" + value + "} is not set or empty");
        return false;
    }
}

/**
 * validateMobilePhone()
 * @desc - Validates a UK Mobile Number
 * @param {String} value        - Value to be validated
 * @return {boolean}
 */
function validateMobilePhone(value) {
    // Trim the value
    value = $.trim(value);

    // Check input is set and not empty
    if (validateSet(value) && validateNotEmpty(value)) {
        // Check value matches regex
        if (value.match(/^00447\d{9}$|^07\d{9}$|^447\d{9}$|^[+]447\d{9}$/)) {
            // String is valid
            printValidationDebug("Value {" + value + "} is a valid mobile number");
            return true;
        } else {
            printValidationDebug("Value {" + value + "} contains invalid characters");
            return false;
        }
    } else {
        // Value is not set or is empty
        printValidationDebug("Value {" + value + "} is not set or empty");
        return false;
    }
}

/**
 * validatePostcode()
 * @desc - Validates a UK Postcode
 * @param {String} value        - Value to be validated
 * @return {boolean}
 */
function validatePostcode(value) {
    // Trim the value
    value = $.trim(value);

    // Remove any spaces in the middle and add a space where needed
    value = value.replace(/ /g, '');
    value = value.replace(/(?=.{3}$)/, ' ');

    // Check input is set and not empty
    if (validateSet(value) && validateNotEmpty(value)) {
        // Check value matches regex
        if (value.match(/^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) [0-9][A-Za-z]{2})$/)) {
            // String is valid
            printValidationDebug("Value {" + value + "} is a valid postcode");
            return true;
        } else {
            printValidationDebug("Value {" + value + "} contains invalid characters");
            return false;
        }
    } else {
        // Value is not set or is empty
        printValidationDebug("Value {" + value + "} is not set or empty");
        return false;
    }
}
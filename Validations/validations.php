<?php

/**
 * ----------------------------------------------------------------
 * PHP Validation Model
 * ----------------------------------------------------------------
 * @author - Jorden Powley <jorden.powley.webdev@gmail.com>
 */

class Validations{

    /**
     * validateSet()
     * @desc - Checks that the value is set (Does not equal NULL OR EMPTY)
     * @param {String} $value       - Value to be checked
     * @return {boolean}
     */
    public function validateSet($value){
        // Check value
        if(isset($value) && !empty($value)){
            return true;
        } else{
            return false;
        }
    }

    /**
     * validateNumeric()
     * @desc - Checks that the value is numeric
     * @param {int} $value      - The value to be checked
     * @return {boolean}
     */
    public function validateNumeric($value){
        // Trim the value
        $value = trim($value);

        // Check value is set and not empty
        if($this->validateSet($value)){
            // Check value is numeric
            if(is_numeric($value)){
                // Value is numeric
                return true;
            } else{
                // Value is not numeric
                return false;
            }
        } else{
            // Value is not set or is empty
            return false;
        }
    }

    /**
     * validateEmail()
     * @desc - Validate an email address
     * @param {String} $value       - The value to be checked
     * @return {boolean}
     */
    public function validateEmail($value){
        // Trim the value
        $value = trim($value);

        // Check value is set and not empty
        if($this->validateSet($value)){
            // Check value is numeric
            if(filter_var($value, FILTER_VALIDATE_EMAIL)){
                // Value is an email address
                return true;
            } else{
                // Value is not numeric
                return false;
            }
        } else{
            // Value is not set or is empty
            return false;
        }
    }

    /**
     * validateHTTP()
     * @desc - Validate an HTTP/HTTPS Link
     * @param {String} $value       - Value to be validated
     * @return {boolean}
     */
    public function validateHTTP($value){
        // Trim the value
        $value = trim($value);

        // Check value is set and not empty
        if($this->validateSet($value)){
            if(substr($value, 0, 7) === "http://"){
                // String is a valid HTTP
                return true;
            } else if(substr($value, 0, 8) === "https://"){
                // String is a valid HTTPS
                return true;
            } else{
                // String is not valid address
                return false;
            }
        } else{
            // Value is not set or is empty
            return false;
        }
    }

    /**
     * validateHTTPS()
     * @desc - Validate a HTTPS link
     * @param {String} $value       - Value to be validated
     * @return {boolean}
     */
    public function validateHTTPS($value){
        // Trim the value
        $value = trim($value);

        // Check value is set and not empty
        if($this->validateSet($value)){
            if(substr($value, 0, 8) === "https://"){
                // String is a valid HTTPS
                return true;
            } else{
                // String is not valid address
                return false;
            }
        } else{
            // Value is not set or is empty
            return false;
        }
    }

    /**
     * validatePasswordStrength()
     * @desc - Validates the strength of a password
     * @param {String} $value       - Value to be validated
     * @param {int} $level          - Password stength required, Defaults to 0
     *                              - [0] alphanumeric minimum
     *                              - [1] alphanumeric, uppercase minimum
     *                              - [2] alphanumeric, uppercase, symbols minimum 
     * @param {int} $length         - Length of password characters, Defaults to 4
     * @return {boolean}
     */
    public function validatePasswordStrength($value, $level = 0, $length = 4){
        // Trim the value
        $value = trim($value);
        // REGEXs
        $anyCaseRegex = "/[a-zA-Z]/";
        $lowerCaseRegex = "/[a-z]/";
        $upperCaseRegex = "/[A-Z]/";
        $numericRegex = "/[0-9]/";
        $symbolRegex = "/[$&+,:;=?@#|'<>.\-^*()%!]/";

        // Check value is set and not empty
        if($this->validateSet($value)){
            // Check length
            if(strlen($value) >= $length){
                // Check for lowercase letter
                preg_match_all($anyCaseRegex, $value, $matches, PREG_SET_ORDER, 0);
                if($this->validateSet($matches)){
                    // Check for numeric
                    preg_match_all($numericRegex, $value, $matches, PREG_SET_ORDER, 0);
                    if($this->validateSet($matches)){
                        // Check if a stronger password is required
                        if($level >= 1){
                            // Check for uppercase letter
                            preg_match_all($upperCaseRegex, $value, $matches, PREG_SET_ORDER, 0);
                            if($this->validateSet($matches)){
                                // Check for lowercase letter
                                preg_match_all($lowerCaseRegex, $value, $matches, PREG_SET_ORDER, 0);
                                if($this->validateSet($matches)){
                                    // Check if a stronger password is required
                                    if($level >= 2){
                                        // Check for symbols
                                        preg_match_all($symbolRegex, $value, $matches, PREG_SET_ORDER, 0);
                                        if($this->validateSet($matches)){
                                            // Password is strong enough
                                            return true;
                                        } else{
                                            // Password does not contain a symbol
                                            return false;
                                        }
                                    } else{
                                        // Required strength met -> Return
                                        return true;
                                    }
                                } else{
                                    // Password does not contain an lowercase letter
                                    return false;
                                }
                            } else{
                                // Password does not contain an uppercase letter
                                return false;
                            }
                        } else{
                            // Required strength met -> Return
                            return true;
                        }
                    } else{
                        // Password does not contain a number
                        return false;
                    }
                } else{
                    // Password does not contain a letter
                    return false;
                }
            } else{
                // Password not long enough
                return false;
            }
        } else{
            // Value is not set or is empty
            return false;
        }
    }

    /**
     * validateInArray()
     * @desc - Validates a value to be in array
     * @param {String} $value       - Value to be validated
     * @param {Object[]} $array     - Array to be checked against
     * @return {boolean}
     */
    public function validateInArray($value, $array){
        // Trim the value
        $value = trim($value);

        // Check value is set and not empty
        if($this->validateSet($value)){
            // Check if in array
            if(in_array($value, $array)){
                // Value in array
                return true;
            } else{
                // Value not in array
                return false;
            }
        } else{
            // Value is not set or is empty
            return false;
        }
    }

    /**
     * validateTitle()
     * @desc - Validate a title
     * @param {String} $value       - Title to be validated
     * @return {boolean}
     */
    public function validateTitle($value){
        // Trim the value
        $value = trim($value);

        $validTitles = ["MR", "MRS", "MISS", "MASTER", "MS", "MX", "MADAM", "DR", "MADDAM"];

        // Check value is set and not empty
        if($this->validateSet($value)){
            // Check if in array
            if($this->validateInArray($value, $validTitles)){
                // Value is a title
                return true;
            } else{
                // Value is not a title
                return false;
            }
        } else{
            // Value is not set or is empty
            return false;
        }
    }

    /**
     * validateAlphaNumeric()
     * @desc - Non-Strict version of alphanumeric validation
     *                              => lowercase alphabet
     *                              => uppercase alphabet
     *                              => numbers
     *                              => whitespace
     *                              => symbols [-] [_]
     * @param {String} $value       - Value to be validated
     * @return {boolean}
     */
    function validateAlphaNumeric($value){
        // Trim the value
        $value = trim($value);

        // Check value is set and not empty
        if($this->validateSet($value)){
            // Check if in array
            preg_match_all("/^[a-zA-Z0-9\s\-\_]+$/", $value, $matches, PREG_SET_ORDER, 0);
            if($this->validateSet($matches)){
                // Value is a suitable alphanumeric
                return true;
            } else{
                // Value is not a suitable alphanumeric
                return false;
            }
        } else{
            // Value is not set or is empty
            return false;
        }
    }

    /**
     * validateAlphaNumericStrict()
     * @desc - Strict version of alphanumeric validation
     *                              => lowercase alphabet
     *                              => uppercase alphabet
     *                              => numbers
     *                              => symbols [-] [_]
     * @param {String} $value       - Value to be validated
     * @return {boolean}
     */
    public function validateAlphaNumericStrict($value){
        // Trim the value
        $value = trim($value);

        // Check value is set and not empty
        if($this->validateSet($value)){
            // Check if in array
            preg_match_all("/^[a-zA-Z0-9\-\_]+$/", $value, $matches, PREG_SET_ORDER, 0);
            if($this->validateSet($matches)){
                // Value is a suitable alphanumeric
                return true;
            } else{
                // Value is not a suitable alphanumeric
                return false;
            }
        } else{
            // Value is not set or is empty
            return false;
        }
    }

    /**
     * validateText()
     * @desc - Validates a block of text
     *                              => lowercase alphabet
     *                              => uppercase alphabet
     *                              => numbers
     *                              => symbols [-] [_] [s] [!] ["] [£] [$] [%] [^] [&] [*] [(] [)] [{] [}] [[] []] ['] [@] [#] [~] [,] [<] [.] [>] [/] [?] [|] [\]
     * @param {String} $value       - Value to be validated
     * @return {boolean}
     */
    public function validateText($value){
        // Trim the value
        $value = trim($value);

        // Check value is set and not empty
        if($this->validateSet($value)){
            // Check if in array
            preg_match_all("/^[a-zA-Z0-9\-\_\s\!\"\£\$\%\^\&\*\(\)\{\}\[\]\'\@\#\~\,\<\.\>\/\?\\\|]+$/", $value, $matches, PREG_SET_ORDER, 0);
            if($this->validateSet($matches)){
                // Value is suitable text
                return true;
            } else{
                // Value is not suitable text
                return false;
            }
        } else{
            // Value is not set or is empty
            return false;
        }
    }

    /**
     * validateBirthday()
     * @desc - Validates a date for birthday
     * @param {String} $day    - Day to be validated
     * @param {String} $month  - Month to be validated
     * @param {String} $year   - Year to be validated
     * @return {boolean}
     */
    function validateBirthday($day, $month, $year){
        $days31 = [1, 3, 5, 7, 8, 10, 12];
        $days30 = [4, 6, 9, 11];
        $startYear = 1900;
        $endYear = 3000;

        // Check inputs are set and not empty
        if($this->validateSet($day) && $this->validateSet($month) && $this->validateSet($year)) {
            // Check each input is numeric
            if(is_numeric($day) && is_numeric($month) && is_numeric($year)){
                // Check dates fit within range
                $day = intval($day);
                $month = intval($month);
                $year = intval($year);

                // Check year input
                if(($year >= $startYear) && ($year <= $endYear)){
                    // Check month input 
                    if(($month > 0) && ($month <= 12)){
                        // Check day input
                        if($this->validateInArray($month, $days31)){
                            // Month has 31 days
                            if(($day > 0) && ($day <= 31)){
                                // Day is valid
                                return true;
                            } else{
                                // Day is not valid 
                                return false;
                            }
                        } else if($this->validateInArray($month, $days30)){
                            // Month has 30 days
                            if(($day > 0) && ($day <= 30)){
                                // Day is valid
                                return true;
                            } else{
                                // Day is not valid 
                                return false;
                            }
                        } else{
                            // Month is Feb
                            if(($day > 0) && ($day <= 29)){
                                // Day is valid
                                return true;
                            } else{
                                // Day is not valid 
                                return false;
                            }
                        }
                    } else{
                        // Month is not valid
                        return false;
                    }
                } else{ 
                    // Year is not valid
                    return false;
                }
            } else{
                // Input is not numeric
                return false;
            }
        } else{
            // Not set or empty
            return false;
        }
    }

    /**
     * validateHomePhone()
     * @desc - Validates a UK Home Phone Number
     * @param {String} value        - Value to be validated
     * @return {boolean}
     */
    public function validateHomePhone($value){
        // Trim the value
        $value = trim($value);

        // Check value is set and not empty
        if($this->validateSet($value)){
            // Check if in array
            preg_match_all("/^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/", $value, $matches, PREG_SET_ORDER, 0);
            if($this->validateSet($matches)){
                // Value is valid home phone
                return true;
            } else{
                // Value is not a valid home phone
                return false;
            }
        } else{
            // Value is not set or is empty
            return false;
        }
    }

    /**
     * validateMobilePhone()
     * @desc - Validates a UK Mobile Number
     * @param {String} value        - Value to be validated
     * @return {boolean}
     */
    public function validateMobilePhone($value){
        // Trim the value
        $value = trim($value);

        // Check value is set and not empty
        if($this->validateSet($value)){
            // Check if in array
            preg_match_all("/^00447\d{9}$|^07\d{9}$|^447\d{9}$|^[+]447\d{9}$/", $value, $matches, PREG_SET_ORDER, 0);
            if($this->validateSet($matches)){
                // Value is valid mobile phone
                return true;
            } else{
                // Value is not a valid mobile phone
                return false;
            }
        } else{
            // Value is not set or is empty
            return false;
        }
    }

    /**
     * validatePostcode()
     * @desc - Validates a UK Postcode
     * @param {String} value        - Value to be validated
     * @return {boolean}
     */
    public function validatePostcode($value){
        // Trim the value
        $value = trim($value);

        // Remove any spaces in the middle and add a space where needed
        $value = preg_replace('/\s/', '', $value);
        $value = substr($value, 0, -3) . " " . substr($value, -3);

        // Check value is set and not empty
        if($this->validateSet($value)){
            // Check if in array
            preg_match_all("/^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) [0-9][A-Za-z]{2})$/", $value, $matches, PREG_SET_ORDER, 0);
            if($this->validateSet($matches)){
                // Value is valid postcode
                return true;
            } else{
                // Value is not a valid postcode
                return false;
            }
        } else{
            // Value is not set or is empty
            return false;
        }
    }

    /**
     * validateBoolean()
     * @desc - Validates a value to be boolean
     * @param {boolean} $value      - Value to be validated
     * @return {boolean}
     */
    public function validateBoolean($value){
        // Check value is set and not empty
        if(($value === true) || ($value === "true") || ($value === "1") || ($value === 1) || ($value === false) || ($value === "false") || ($value === "0") || ($value === 0)){
            // Value is a valid boolean
            return true;
        } else{
            // Value is not set or is empty
            return false;
        }
    }

    /**
     * validateCSSColor()
     * @desc - Validates a user input string to be a valid CSS color
     * @param {String} $value       - Value to be validated
     * @return {boolean}
     */
    public function validateCSSColor($value){
        // Function variables
        $wordedColors = ["aliceblue", "antiquewhite", "aqua", "aquamarine", "azure", "beige", "bisque", "black", "blanchedalmond", "blue", "blueviolet", "brown", "burlywood", "cadetblue", "chartreuse", "chocolate", "coral", "cornflowerblue", "cornsilk", "crimson", "cyan", "darkblue", "darkcyan", "darkgoldenrod", "darkgray", "darkgreen", "darkkhaki", "darkmagenta", "darkolivegreen", "darkorange", "darkorchid", "darkred", "darksalmon", "darkseagreen", "darkslateblue", "darkslategray", "darkturquoise", "darkviolet", "deeppink", "deepskyblue", "dimgray", "dodgerblue", "firebrick", "floralwhite", "forestgreen", "fuchsia", "gainsboro", "ghostwhite", "gold", "goldenrod", "gray", "green", "greenyellow", "honeydew", "hotpink", "indianred", "indigo", "ivory", "khaki", "lavender", "lavenderblush", "lawngreen", "lemonchiffon", "lightblue", "lightcoral", "lightcyan", "lightgoldenrodyellow", "lightgray", "lightgreen", "lightpink", "lightsalmon", "lightseagreen", "lightskyblue", "lightslategray", "lightsteelblue", "lightyellow", "lime", "limegreen", "linen", "magenta", "maroon", "mediumaquamarine", "mediumblue", "mediumorchid", "mediumpurple", "mediumseagreen", "mediumslateblue", "mediumspringgreen", "mediumturquoise", "mediumvioletred", "midnightblue", "mintcream", "mistyrose", "moccasin", "navajowhite", "navy", "oldlace", "olive", "olivedrab", "orange", "orangered", "orchid", "palegoldenrod", "palegreen", "paleturquoise", "palevioletred", "papayawhip", "peachpuff", "peru", "pink", "plum", "powderblue", "purple", "rebeccapurple", "red", "rosybrown", "royalblue", "saddlebrown", "salmon", "sandybrown", "seagreen", "seashell", "sienna", "silver", "skyblue", "slateblue", "slategray", "snow", "springgreen", "steelblue", "tan", "teal", "thistle", "tomato", "turquoise", "violet", "wheat", "white", "whitesmoke", "yellow", "yellowgreen"];
        $hexREGEX = "/^#[0-9A-Fa-f]{6}$|^#[0-9A-Fa-f]{3}$/";
        $rgbaREGEX = "/rgba\((\d{1}|\d{2}|\d{3}),(\d{1}|\d{2}|\d{3}),(\d{1}|\d{2}|\d{3}),(\d{1}|\d{1}.\d{1}|\d{1}.\d{2})\)/";
        $rgbREGEX = "/rgb\((\d{1}|\d{2}|\d{3}),(\d{1}|\d{2}|\d{3}),(\d{1}|\d{2}|\d{3})\)/";

        // Remove whitespace from the value and convert to lowercase
        $value = preg_replace("\s", "", $value);
        $value = strtolower($value);

        // Check first characters and split validation dependent
        if(substr($value, 0, 1) === "#"){
            // String is a hex code
            // Check the string length is 4 OR 7 only
            if((strlen($value) === 4) || (strlen($value) === 7)){
                // Check correct characters 
                preg_match_all($hexREGEX, $value, $matches, PREG_SET_ORDER, 0);
                if($this->validateSet($matches)){
                    return true;
                } else{
                    // Hex uses invalid characters
                    return false;
                }
            } else{
                // Hex is not of valid length
                return false;
            }
        } else if(substr($value, 0, 5) === "rgba("){
            // String is a rgba
            // Check the RGB string has the correct character structure
            preg_match_all($rgbaREGEX, $value, $matches, PREG_SET_ORDER, 0);
            if($this->validateSet($matches)){
                // Remove un-needed characters
                $value = preg_replace("rgba(", "", $value);
                $value = preg_replace(")", "", $value);

                // Breakout number values
                $numbers = explode(",", $value);

                // Check each match is between 0 - 255 and 0 - 1
                // Check first number
                if((intval($numbers[0]) >= 0) && (intval($numbers[0]) <= 255)){
                    // Check second number
                    if((intval($numbers[1]) >= 0) && (intval($numbers[1]) <= 255)){
                        // Check third number
                        if((intval($numbers[2]) >= 0) && (intval($numbers[2]) <= 255)){
                            // Check the fourth number
                            if((intval($numbers[3]) >= 0) && (intval($numbers[3]) <= 1)){
                                // Value is a valid RGBA color
                                return true;
                            } else{
                                // RGBA fourth number is not in range
                                return false;
                            }
                        } else{
                            // RGBA third number is not in range
                            return false;
                        }
                    } else{
                        // RGBA second number is not in range
                        return false;
                    }
                } else{
                    // RGBA first number is not in range
                    return false;
                }
            } else{
                // RGBA string does not have the correct structure
                return false;
            }
        } else if(substr($value, 0, 4) === "rgb("){
            // String is a rgb
            // Check the RGB string has the correct character structure
            preg_match_all($rgbREGEX, $value, $matches, PREG_SET_ORDER, 0);
            if($this->validateSet($matches)){
                // Remove un-needed characters
                $value = preg_replace("rgb(", "", $value);
                $value = preg_replace(")", "", $value);

                // Breakout number values
                $numbers = explode(",", $value);

                // Check each match is between 0 - 255
                // Check first number
                if((intval($numbers[0]) >= 0) && (intval($numbers[0]) <= 255)){
                    // Check second number
                    if((intval($numbers[1]) >= 0) && (intval($numbers[1]) <= 255)){
                        // Check third number
                        if((intval($numbers[2]) >= 0) && (intval($numbers[2]) <= 255)){
                            // Value is valid RGB CSS
                            return true;
                        } else{
                            // RGB third number is not in range
                            return false;
                        }
                    } else{
                        // RGB second number is not in range
                        return false;
                    }
                } else{
                    // RGB first number is not in range
                    return false;
                }
            } else{
                // RGB string does not have the correct structure
                return false;
            }
        } else if($this->validateInArray($value, $wordedColors)){
            // String is a worded color
            return true;
        } else{
            // String is not valid
            return false;
        }
    }
}

?>
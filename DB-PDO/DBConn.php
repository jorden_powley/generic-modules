<?php
/**
 * ------------------------------------------------
 * DBConn
 * @author Jorden Powley
 * A barebones database connection class
 * ------------------------------------------------
 */

class DBConn extends PDO
{
    /**
     * The database connection host
     * @var string
     */
    private $dbHost;

    /**
     * The database connection port
     * @var string
     */
    private $dbPort;

    /**
     * The database connection user
     * @var string
     */
    private $dbUser;

    /**
     * The database connection password
     * @var string
     */
    private $dbPass;

    /**
     * The database name
     * @var string
     */
    private $dbName;

    /**
     * Any class errors
     * @var object
     */
    private $error;

    /**
     * __construct()
     * Basic constructor method for the class
     * @param string $dbUser
     * @param string $dbPass
     * @param string $dbName
     * @param string $dbHost
     * @param int $dbPort [optional]
     */
    public function __construct(string $dbUser,
                                string $dbPass,
                                string $dbName = null,
                                string $dbHost,
                                int $dbPort = 3306)
    {
        // Check that all required DB fields have been passed
        $this->dbUser = (isset($dbUser) && !empty($dbUser)) ? $dbUser : die("No DB User has been passed");
        $this->dbPass = (isset($dbPass)) ? $dbPass : die("No DB Pass has been passed");
        $this->dbName = (isset($dbName)) ? $dbName: die("No DB name has been passed");
        $this->dbHost = (isset($dbHost) && !empty($dbHost)) ? $dbHost : die("No DB Host has been passed");
        $this->dbPort = (isset($dbPort) && !empty($dbPort)) ? $dbPort : die("No DB Port has been passed");

        // Create the connection string
        $dsn = "mysql:host={$this->dbHost}:{$this->dbPort};dbname={$this->dbName}";

        // Create a new PDO instance
        try {
            parent::__construct($dsn, $this->dbUser, $this->dbPass);
        } catch (PDOException $exception) {
            $this->error = $exception->getMessage();
        }
    } # END _construct()
}
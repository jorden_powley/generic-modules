# DB-PDO
An easy to use codeset for speedy integration of a PHP PDO database class.

## Setup
* Requires copying into the required directory for usage.
* Requires the following values being passed in order:
```
string  $dbUser             - The username
string  $dbPass             - The user password
string  $dbName             - The DB name
string  $dbHost             - The DB host
int     $dbPort [optional]  - The DB port, defaults to 3306
```

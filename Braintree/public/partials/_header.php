<?php

/**
 * ------------------------------------------------
 * Braintree - Header Partial
 * ------------------------------------------------
 */

?>
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
  <h5 class="my-2 mr-md-auto font-weight-normal">Braintree Gateway</h5>
  <a class="btn btn-outline-primary" href="/">Home</a>
</div>
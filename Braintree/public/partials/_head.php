<?php

/**
 * ------------------------------------------------
 * Braintree - Head Partial
 * ------------------------------------------------
 */

?>
<!DOCTYPE html>
<!--[if lte IE 6]><html class="preIE7 preIE8 preIE9" lang="en"><![endif]-->
<!--[if IE 7]><html class="preIE8 preIE9" lang="en"><![endif]-->
<!--[if IE 8]><html class="preIE9" lang="en"><![endif]-->
<!--[if gte IE 9]><!--><html lang="en"><!--<![endif]-->
<head>
    <title>Braintree</title>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <meta name="author" content="Jorden Powley">
    <meta name="description" content="Braintree online payment form example">
    <meta name="keywords" content="braintree,online,payment,gateway,js,in-page">

    <!-- Load In Required Stylesheet  -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

    <!-- Load In Required Scripting -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

    <!-- Load In Braintree Scripting -->
    <script src="https://js.braintreegateway.com/web/dropin/1.13.0/js/dropin.min.js"></script>
    <script src="js/braintree.js"></script>
</head>
<body>
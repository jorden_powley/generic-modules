/**
 * ------------------------------------------------
 * Braintree Gateway Scripting
 * ------------------------------------------------
 * @desc - Client side scripting required by 
 *         Braintree
 * @requires - JQuery
 * ------------------------------------------------
 */

 /**
  * On Document Ready
  */
$(document).ready(function(){
    /**
     * Braintree Loading functions
     */
    var button = document.querySelector('#submit-button');

    braintree.dropin.create({
        authorization: 'CLIENT_TOKEN_FROM_SERVER',
        container: '#dropin-container'
    }, function (createErr, instance) {
        button.addEventListener('click', function () {
            instance.requestPaymentMethod(function (err, payload) {
                // Submit payload.nonce to your server
            });
        });
    });
});
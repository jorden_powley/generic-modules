<?php

/**
 * ------------------------------------------------
 * Braintree Index Page
 * ------------------------------------------------
 * @desc - Display the basic Braintree transaction 
 *         form.
 * ------------------------------------------------
 */

 // Pull config
$config;
if(!file_exists("../config.json")){
    die("No configuration file");
} else{
    $config = json_decode(file_get_contents("../config.json"), true);
}

// Default the required values
// $config['BRAINTREE']['']

?>

<?php include "partials/_head.php" ?>
<?php include "partials/_header.php" ?>
<div class="px-3 py-3 pb-md-5 mb-10 mx-auto text-center">
    <div class="lead">Please fill out the below form</div>
</div>
<div class="container" style="max-width: 600px;">
    <div id="dropin-container"></div>
    <button id="submit-button" class="btn btn-primary mt-auto">Request payment method</button>
</div>
<?php include "partials/_footer.php" ?>
<?php include "partials/_foot.php" ?>
/**
 * ----------------------------------------------------------------
 * JS Notification Functions
 * ----------------------------------------------------------------
 * @author - Jorden Powley <jorden.powley.webdev@gmail.com>
 * @requires - jQuery
 */

// ----------------------------------------------------------------
// ------------------------Global Variables------------------------
// ----------------------------------------------------------------
var DEFAULT_ERROR_COLOR = "red";
var DEFAULT_WARNING_COLOR = "orangered";
var DEFAULT_SUCCESS_COLOR = "green";

var notificationDebug = false;

var notificationFade = false;
var notificationTimer = 5000; // Default timeout is 5 seconds => 5000

var errorNotificationColor;
var warningNotificationColor;
var successNotificationColor;

// ----------------------------------------------------------------
// ----------------------Validation Functions----------------------
// ----------------------------------------------------------------

/**
 * validateCSSColor()
 * @desc - Validates a user input string to be a valid CSS color
 * @param {String} value        - String to be validated
 * @return {boolean}
 */
function validateCSSColor(value) {
    // Function variables
    var wordedColors = ["aliceblue", "antiquewhite", "aqua", "aquamarine", "azure", "beige", "bisque", "black", "blanchedalmond", "blue", "blueviolet", "brown", "burlywood", "cadetblue", "chartreuse", "chocolate", "coral", "cornflowerblue", "cornsilk", "crimson", "cyan", "darkblue", "darkcyan", "darkgoldenrod", "darkgray", "darkgreen", "darkkhaki", "darkmagenta", "darkolivegreen", "darkorange", "darkorchid", "darkred", "darksalmon", "darkseagreen", "darkslateblue", "darkslategray", "darkturquoise", "darkviolet", "deeppink", "deepskyblue", "dimgray", "dodgerblue", "firebrick", "floralwhite", "forestgreen", "fuchsia", "gainsboro", "ghostwhite", "gold", "goldenrod", "gray", "green", "greenyellow", "honeydew", "hotpink", "indianred", "indigo", "ivory", "khaki", "lavender", "lavenderblush", "lawngreen", "lemonchiffon", "lightblue", "lightcoral", "lightcyan", "lightgoldenrodyellow", "lightgray", "lightgreen", "lightpink", "lightsalmon", "lightseagreen", "lightskyblue", "lightslategray", "lightsteelblue", "lightyellow", "lime", "limegreen", "linen", "magenta", "maroon", "mediumaquamarine", "mediumblue", "mediumorchid", "mediumpurple", "mediumseagreen", "mediumslateblue", "mediumspringgreen", "mediumturquoise", "mediumvioletred", "midnightblue", "mintcream", "mistyrose", "moccasin", "navajowhite", "navy", "oldlace", "olive", "olivedrab", "orange", "orangered", "orchid", "palegoldenrod", "palegreen", "paleturquoise", "palevioletred", "papayawhip", "peachpuff", "peru", "pink", "plum", "powderblue", "purple", "rebeccapurple", "red", "rosybrown", "royalblue", "saddlebrown", "salmon", "sandybrown", "seagreen", "seashell", "sienna", "silver", "skyblue", "slateblue", "slategray", "snow", "springgreen", "steelblue", "tan", "teal", "thistle", "tomato", "turquoise", "violet", "wheat", "white", "whitesmoke", "yellow", "yellowgreen"];
    var hexREGEX = /^#[0-9A-Fa-f]{6}$|^#[0-9A-Fa-f]{3}$/i;
    var rgbaREGEX = /rgba\((\d{1}|\d{2}|\d{3}),(\d{1}|\d{2}|\d{3}),(\d{1}|\d{2}|\d{3}),(\d{1}|\d{1}.\d{1}|\d{1}.\d{2})\)/g;
    var rgbREGEX = /rgb\((\d{1}|\d{2}|\d{3}),(\d{1}|\d{2}|\d{3}),(\d{1}|\d{2}|\d{3})\)/g;

    // Remove whitespace from the value and convert to lowercase
    value = value.replace(/\s/g, '');
    value = value.toLowerCase();

    // Check first characters and split validation dependent
    if (value.startsWith("#")) {
        // String is a hex code
        // Check the string length is 4 OR 7 only
        if ((value.length === 4) || (value.length === 7)) {
            // Check correct characters 
            if (hexREGEX.test(value)) {
                // Valid
                return true;
            } else {
                // Invalid
                return false;
            }
        } else {
            // Invalid
            return false;
        }
    } else if (value.startsWith("rgba(")) {
        // String is a rgba
        // Check the RGB string has the correct character structure
        if (rgbaREGEX.test(value)) {
            // Remove un-needed characters
            value = value.replace("rgba(", "");
            value = value.replace(")", "");

            // Breakout number values
            var matches = value.split(",");

            // Check each match is between 0 - 255 and 0 - 1
            // Check first number
            if ((parseInt(matches[0]) >= 0) && (parseInt(matches[0]) <= 255)) {
                // Check second number
                if ((parseInt(matches[1]) >= 0) && (parseInt(matches[1]) <= 255)) {
                    // Check third number
                    if ((parseInt(matches[2]) >= 0) && (parseInt(matches[2]) <= 255)) {
                        // Check the fourth number
                        if ((parseInt(matches[3]) >= 0) && (parseInt(matches[3]) <= 1)) {
                            // Valid
                            return true;
                        } else {
                            // Invalid
                            return false;
                        }
                    } else {
                        // Invalid
                        return false;
                    }
                } else {
                    // Invalid
                    return false;
                }
            } else {
                // Invalid
                return false;
            }
        } else {
            // RGB string does not have the correct structure
            json['SUCCESS'] = false;
            json['MESSAGE'] = "(" + name + ") RGBA input does not have the correct structure. It should be in the following format: rgba([0 - 255],[0 - 255],[0 - 255],[0 - 1])";
            json['TYPE'] = "RGBA";
        }
    } else if (value.startsWith("rgb(")) {
        // String is a rgb
        // Check the RGB string has the correct character structure
        if (rgbREGEX.test(value)) {
            // Remove un-needed characters
            value = value.replace("rgb(", "");
            value = value.replace(")", "");

            // Breakout number values
            var matches = value.split(",");

            // Check each match is between 0 - 255
            // Check first number
            if ((parseInt(matches[0]) >= 0) && (parseInt(matches[0]) <= 255)) {
                // Check second number
                if ((parseInt(matches[1]) >= 0) && (parseInt(matches[1]) <= 255)) {
                    // Check third number
                    if ((parseInt(matches[2]) >= 0) && (parseInt(matches[2]) <= 255)) {
                        // Valid
                        return true;
                    } else {
                        // Invalid
                        return false;
                    }
                } else {
                    // Invalid
                    return false;
                }
            } else {
                // Invalid
                return false;
            }
        } else {
            // Invalid
            return false;
        }
    } else if ($.inArray(value, wordedColors) !== -1) {
        // Valid
        return true;
    } else {
        // Invalid
        return false;
    }
    // END validateCSSColor()
}

// ----------------------------------------------------------------
// ------------------On Ready Function With Events-----------------
// ----------------------------------------------------------------
$(document).ready(function () {
    // Detect click on the close button
    $('body').on('click', '#notification_close', function (event) {
        event.stopPropagation();

        // Remove notification bar
        closeNotificationBar();
    })
});

// ----------------------------------------------------------------
// ---------------------Notification Functions---------------------
// ----------------------------------------------------------------


/**
 * setNotificationDebug()
 * @desc - Sets debug on/off -> Defaults to false
 * @param {String/boolean} status       - Debug status to be set
 */
function setNotificationDebug(status) {
    // Parse the input status
    if ((status === true) || (status === "true") || (status === "1") || (status === 1)) {
        // Debug on
        notificationDebug = true;
    } else {
        // Debug off
        notificationDebug = false;
    }
}

/**
 * setNotificationColors()
 * @desc - Sets the three colour states of the notification bar
 *          To be called before creation
 * @param {String} inputErrorNotificationColor       - Background color for the error bar
 * @param {String} inputSuccessNotificationColor     - Background color for the success bar
 * @param {String} inputWarningNotificationColor     - Background color for the warning bar (Optional)
 */
function setNotificationColors(inputErrorNotificationColor, inputSuccessNotificationColor, inputWarningNotificationColor) {
    // Validate the error color -> remove whitespace and lowercase the string
    inputErrorNotificationColor = inputErrorNotificationColor.replace(/\s/g, '');
    inputErrorNotificationColor = inputErrorNotificationColor.toLowerCase();

    if (validateCSSColor(inputErrorNotificationColor) === true) {
        // Set the error color
        errorNotificationColor = inputErrorNotificationColor;
    } else {
        // Error color is not valid -> Check debug for print output
        if (notificationDebug === true) {
            console.log("setNotificationColors: (errorNotificationColor){" + inputErrorNotificationColor + "} Is not a valid CSS color.");
        }
    }

    // Validate the success color -> remove whitespace and lowercase the string
    inputSuccessNotificationColor = inputSuccessNotificationColor.replace(/\s/g, '');
    inputSuccessNotificationColor = inputSuccessNotificationColor.toLowerCase();

    if (validateCSSColor(inputSuccessNotificationColor) === true) {
        // Set the success color
        successNotificationColor = inputSuccessNotificationColor;
    } else {
        // Success color is not valid -> Check debug for print output
        if (notificationDebug === true) {
            console.log("setNotificationColors: (successNotificationColor){" + inputSuccessNotificationColor + "} Is not a valid CSS color.");
        }
    }

    // Check warning is set -> IF set validate IF NOT -> Ignore
    if ((inputWarningNotificationColor !== undefined) && (inputWarningNotificationColor !== null) && (inputWarningNotificationColor !== "")) {
        // Validate the warning color -> remove whitespace and lowercase the string
        inputWarningNotificationColor = inputWarningNotificationColor.replace(/\s/g, '');
        inputWarningNotificationColor = inputWarningNotificationColor.toLowerCase();

        if (validateCSSColor(inputWarningNotificationColor) === true) {
            // Set the warning color
            warningNotificationColor = inputWarningNotificationColor;
        } else {
            // Warning color is not valid -> Check debug for print output
            if (notificationDebug === true) {
                console.log("setNotificationColors: (warningNotificationColor){" + inputWarningNotificationColor + "} Is not a valid CSS color.");
            }
        }
    }

}

/**
 * createNotificationBar()
 * @desc - Appends a notification bar to the page - This bar isn't 
 *         visible
 */
function createNotificationBar() {
    // Build up bar structure
    var $notificationBar = $('<div>', {
        id: "notification_bar"
    });

    $notificationBar.append($('<div>', {
        id: "notification_code",
        text: 'CODE'
    }));
    $notificationBar.append($('<div>', {
        id: "notification_message",
        text: "MESSAGE"
    }));
    $notificationBar.append($('<div>', {
        class: "clearfix"
    }));
    $notificationBar.append($('<div>', {
        id: "notification_close",
        html: "&times;"
    }));

    // Build up css and append to a style tag
    var notificationBarCSS = "#notification_bar, #notification_bar *{box-sizing: border-box; margin: 0; padding: 0;}";
    notificationBarCSS += "#notification_bar{width: 100%; z-index: 200; display: none; position: fixed; top: 0px; left: 0px; padding: 20px; font-size: 0.8rem; text-transform: uppercase;}"

    // Change dependent on whether user has set the background color
    if ((successNotificationColor !== undefined) && (successNotificationColor !== null)) {
        // Use the user's color for the notification bar
        notificationBarCSS += "#notification_bar.success{display: block; color: #FCFCFC; background: " + successNotificationColor + ";}";
    } else {
        // Use the default color
        notificationBarCSS += "#notification_bar.success{display: block; color: #FCFCFC; background: " + DEFAULT_SUCCESS_COLOR + ";}";
    }
    if ((warningNotificationColor !== undefined) && (warningNotificationColor !== null)) {
        // Use the user's color for the notification barbar with
        notificationBarCSS += "#notification_bar.warning{display: block; color: #FCFCFC; background: " + warningNotificationColor + ";}";
    } else {
        // Use the default color
        notificationBarCSS += "#notification_bar.warning{display: block; color: #FCFCFC; background: " + DEFAULT_WARNING_COLOR + ";}";
    }
    if ((errorNotificationColor !== undefined) && (errorNotificationColor !== null)) {
        // Use the user's color for the notification bar
        notificationBarCSS += "#notification_bar.error{display: block; color: #FCFCFC; background: " + errorNotificationColor + ";}";
    } else {
        // Use the default color
        notificationBarCSS += "#notification_bar.error{display: block; color: #FCFCFC; background: " + DEFAULT_ERROR_COLOR + ";}";
    }

    notificationBarCSS += "#notification_code{width: 10%; display: block; position: relative; float: left;}";
    notificationBarCSS += "#notification_message{width: 85%; display: block; position: relative; float: left;}";
    notificationBarCSS += "#notification_close{width: 20px; height: 20px; display: block; position: absolute; top: 50%; right: 20px; -webkit-transform: translateY(-50%); transform: translateY(-50%); font-size: 1.4rem; text-align: right; line-height: 20px; user-select: none;}";
    notificationBarCSS += "#notification_close:hover{cursor: pointer; opacity: 0.6;}";
    notificationBarCSS = $('<style>').html(notificationBarCSS);

    console.log($notificationBar);

    // Append elements to the body
    $('body').append(notificationBarCSS);
    $('body').append($notificationBar);
}

/**
 * openNotificationBar()
 * @desc - Opens the notification bar by applying a class
 * @param {String} status       - Status to open the bar with
 *                                (success, warning, error)
 * @param {String} message      - Message to be displayed
 * @param {String} code         - Code to be inserted (optional)
 */
function openNotificationBar(status, message, code) {
    // Remove all classes from the notification bar
    $('#notification_bar').removeClass('success');
    $('#notification_bar').removeClass('warning');
    $('#notification_bar').removeClass('error');

    // Clearout the current notification bar
    $('#notification_code').empty();
    $('#notification_message').empty();

    status = status.toLowerCase();

    // Check status is valid for this bar
    if ((status === "success") || (status === "warning") || (status === "error")) {
        // Insert text into notification bar
        if ((code !== undefined) && (code !== null)) {
            $('#notification_code').text(code);
        }

        // Check message is defined
        if ((message !== undefined) && (message !== null)) {
            $('#notification_message').text(message);

            // Add class to the notification bar 
            $('#notification_bar').addClass(status);
        } else {
            // Message isn't defined -> Check debug status and print
            if (notificationDebug === true) {
                console.log("Message is not defined and is required for a notification bar");
            }
        }
    } else {
        // Status is not valid -> check if debugging is on
        if (notificationDebug === true) {
            console.log("Invalid class used to open notification bar. Supported {success, warning, error}");
        }
    }
}

/**
 * closeNotificationBar()
 * @desc - Removes active classes and closes the notification bar
 */
function closeNotificationBar() {
    // Remove classes
    $('#notification_bar').removeClass('success');
    $('#notification_bar').removeClass('warning');
    $('#notification_bar').removeClass('error');
}

/**
 * setNotificationFadeOut()
 * @desc - Sets the notification bar to timeout automatically
 * @param {int} time        - Time it takes to fade out (optional, defaults to 5000)
 */
function setNotificationFadeOut(timer){
    // Turn fade option on
    notificationFade = true;

    // Check if timer has been set
    if((timer !== undefined) && (timer !== null) && (timer !== "")){
        // Use inputted value
        notificationTimer = timer;
    }
}
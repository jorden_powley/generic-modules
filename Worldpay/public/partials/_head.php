<?php

/**
 * ------------------------------------------------
 * Worldpay - Head Partial
 * ------------------------------------------------
 */

?>
<!DOCTYPE html>
<!--[if lte IE 6]><html class="preIE7 preIE8 preIE9" lang="en"><![endif]-->
<!--[if IE 7]><html class="preIE8 preIE9" lang="en"><![endif]-->
<!--[if IE 8]><html class="preIE9" lang="en"><![endif]-->
<!--[if gte IE 9]><!--><html lang="en"><!--<![endif]-->
<head>
    <title>Worldpay Online</title>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <meta name="author" content="Jorden Powley">
    <meta name="description" content="Worldpay online payment form example">
    <meta name="keywords" content="worldpay,online,payment,gateway,js,in-page">

    <!-- Load In Required Stylesheet  -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

    <!-- Load in page JS variables -->
    <script>
        // Function Variables
        const worldpayClientKey = "<?php echo $config['WORLDPAY']['CLIENT_KEY'] ?>";
        const worldpayProcess = "<?php echo $config['WORLDPAY']['TRANSACTION_PROCESS'] ?>";
    </script>

    <!-- Load In Required Scripting -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

    <!-- Worldpay Scripting -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.worldpay.com/v1/worldpay.js"></script>
    <script src="js/worldpay.js"></script>
</head>
<body>
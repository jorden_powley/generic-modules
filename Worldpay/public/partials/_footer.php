<?php

/**
 * ------------------------------------------------
 * Worldpay - Footer Partial
 * ------------------------------------------------
 */

?>
<footer class="pt-4 my-md-5 border-top">
    <div>
        <p class="text-center text-muted"><small>Jorden Powley <?php echo date("Y"); ?></small></p>
    </div>
</footer>
<?php

/**
 * ------------------------------------------------
 * Worldpay Index Page
 * ------------------------------------------------
 * @desc - Display the basic Worldpay transaction 
 *         form.
 * ------------------------------------------------
 */

// Pull config
$config;
if(!file_exists("../config.json")){
    die("No configuration file");
} else{
    $config = json_decode(file_get_contents("../config.json"), true);
}

// Pull out required Worldpay values
$config['WORLDPAY']['CLIENT_KEY'] = (array_key_exists('CLIENT_KEY', $config['WORLDPAY']) && isset($config['WORLDPAY']['CLIENT_KEY']) && !empty($config['WORLDPAY']['CLIENT_KEY'])) ? $config['WORLDPAY']['CLIENT_KEY'] : "";
$config['WORLDPAY']['TRANSACTION_PROCESS'] = (array_key_exists('TRANSACTION_PROCESS', $config['WORLDPAY']) && isset($config['WORLDPAY']['TRANSACTION_PROCESS']) && !empty($config['WORLDPAY']['TRANSACTION_PROCESS'])) ? $config['WORLDPAY']['TRANSACTION_PROCESS'] : "";

?>

<?php include "partials/_head.php" ?>
<?php include "partials/_header.php" ?>
<div class="px-3 py-3 pb-md-5 mb-10 mx-auto text-center">
    <div class="lead">Please fill out the below form</div>
</div>
<div class="container" style="max-width: 600px;">
    <form method="POST" action="https://payments.securetrading.net/process/payments/details" id="worldpay_form">
        <!-- 
            User Editable Form Fields
         -->
        <div class="form-group">
            <label for="mainamount">Amount:</label>
            <select name="mainamount" id="mainamount" class="form-control">
                <option value="100">£1</option>
                <option value="500">£5</option>
                <option value="1000">£10</option>
                <option value="2000">£20</option>
            </select>
        </div>
    </form>
    <div id='payment_section'></div>
    <button class="btn btn-primary mt-auto" id="worldpay_submit" onclick="Worldpay.submitTemplateForm()" type="submit">Pay</button>
</div>
<?php include "partials/_footer.php" ?>
<?php include "partials/_foot.php" ?>
/**
 * ------------------------------------------------
 * Worldpay JS Scripting
 * ------------------------------------------------
 * @desc - All required Worldpay JS scripting
 * ------------------------------------------------
 */

/**
 * Scripting constants
 */
const formName = "worldpay_form";
const formLocation = "payment_section";

/**
 * @desc - Initializes the Worldpay payment form, 
 *         sets up callback to lead to processing functions
 */
window.onload = function() {
    Worldpay.useTemplateForm({
        'clientKey': worldpayClientKey,                 // Include client key for token generation
        'form': formName,                               // Form for Worldpay to listen for
        'paymentSection': formLocation,                 // Location to iframe in Worldpay form
        'display': 'inline',
        'templateOptions': {images:{enabled:false}},    // Turns logos off in the Worldpay form
        'saveButton': false,                            // Don't allow details to be saved
        'reusable': false,
        'callback': function(response) {                // Callback function to be used after Worldpay has created a transaction token
            if (response && response.token) {
                var token = response.token;
                var nameOnCard = response.paymentMethod.name;
                processWorldpay(token, nameOnCard);     // Our functions name to be called
            }
        }
    });
}

/**
 * processWorldpay()
 * @desc - Call back function for Worldpay. Uses AJAX 
 *         to our server side processing functions
 * @param {String} token        - Token from Worldpay
 * @param {String} nameOnCard   - Name on card from Worldpay
 */
function processWorldpay(token, nameOnCard) {
    // Force set variables to be passed
    var topupAmount = $('#mainamount').val();
    topupAmount = parseInt(topupAmount);

    // Create data string
    var dataToSend = {
        "topup_amount": topupAmount,
        "token": token,
        "name_on_card": nameOnCard
    }

    // Convert to JSON and print
    dataToSend = JSON.stringify(dataToSend);
    console.log("Data To Process: ");
    console.log(dataToSend);

    // AJAX to process
    $.ajax({
        type: "POST",
        url: worldpayProcess,
        data: {
            values: dataToSend
        },
        success: function (data) {
            console.log("Raw Response From Process: ");
            console.log(data);
            data = JSON.parse(data);
            if (data['SUCCESS'] === true) {
                alert("Payment success");
            } else {
                // Topup process failed
                alert("Payment failed");
            }
        },
        error: function (data) {
            alert("AJAX error");
        }
    });
}
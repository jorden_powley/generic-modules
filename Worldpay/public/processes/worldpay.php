<?php

/**
 * ----------------------------------------------------------------
 * Worldpay Example Process
 * ----------------------------------------------------------------
 * @author - Jorden Powley <jorden.powley.webdev@gmail.com>
 * @requires - Worldpay Model
 */

ini_set('max_execution_time', 0);
date_default_timezone_set("Europe/London");

// Date for logging purposes
$logDate = date("Y-m-d H:i:s");

require_once('../models/WorldpayModel.php');
$worldpayObject = new WorldpayModel;

// GET configuration settings
$config = file_get_contents('../../config.json');
$config = json_decode($config, true);

// ----------------------------------------------------------------
// ------------------------Global Variables------------------------
// ----------------------------------------------------------------
$topupAmount;
$token;
$nameOnCard;

// Process Responses
$worldpayAuthorizeResponse; # Only used if Worldpay fails
$worldpayOrderCode;
$worldpayCaptureResponse; # Only used if Worldpay fails

// Continuations
$postReceived = false;
$dataValid = false;
$worldpayAuthorized = false;
$worldpayCaptured = false;

// ----------------------------------------------------------------
// ----------------------Validation Functions----------------------
// ----------------------------------------------------------------

/**
 * validateSet()
 * @desc - Checks that the value is set (Does not equal NULL OR EMPTY)
 * @param {String} $value       - Value to be checked
 * @return {boolean}
 */
function validateSet($value){
    // Check value
    if(isset($value) && !empty($value)){
        return true;
    } else{
        return false;
    }
}

/**
 * validateNumeric()
 * @desc - Checks that the value is numeric
 * @param {int} $value      - The value to be checked
 * @return {boolean}
 */
function validateNumeric($value){
    // Trim the value
    $value = trim($value);

    // Check value is set and not empty
    if(validateSet($value)){
        // Check value is numeric
        if(is_numeric($value)){
            // Value is numeric
            return true;
        } else{
            // Value is not numeric
            return false;
        }
    } else{
        // Value is not set or is empty
        return false;
    }
}

// ----------------------------------------------------------------
// ------------------------Process Functions-----------------------
// ----------------------------------------------------------------

/**
 * endProcess()
 * @desc - Ends the process and returns the result
 * @param {boolean} $status     - The end status of the process
 * @param {String} $message     - Ending message
 */
function endProcess($status, $message){
    // Build JSON object and encode
    $json = array("SUCCESS" => $status,
                  "MESSAGE" => $message);
    $json = json_encode($json);

    // Return result
    echo $json;
    die();
}

// ----------------------------------------------------------------
// ----------------------------------------------------------------
// --------------------Main Process Starts HERE--------------------
// ----------------------------------------------------------------
// ----------------------------------------------------------------

// ----------------------------------------------------------------
// -----------------------Retrieve POST Data-----------------------
// ----------------------------------------------------------------
if(!isset($_POST) || empty($_POST)){
    // POST not set 
    endProcess(false, "POST not set");
} else{
    // POST is set -> Get data and store
    $_POST = json_decode($_POST['values'], true);

    // Ternary operators
    $topupAmount = (array_key_exists("topup_amount", $_POST) && isset($_POST['topup_amount'])) ? $_POST['topup_amount'] : null;
    $token = (array_key_exists("token", $_POST) && isset($_POST['token'])) ? $_POST['token'] : null;
    $nameOnCard = (array_key_exists("name_on_card", $_POST) && isset($_POST['name_on_card'])) ? $_POST['name_on_card'] : null;

    // Set continuation
    $postReceived = true;
}

// ----------------------------------------------------------------
// -----------------------Validate POST Data-----------------------
// ----------------------------------------------------------------
if($postReceived !== true){
    // Error in retrieving POST
    endProcess(false, "Error in retrieving POST");
} else{
    // Validate fields are set
    if(validateSet($token)){
        // Token is set -> Validate amount
        if(validateSet($topupAmount)){
            if(validateNumeric($topupAmount)){
                // Amount is valid -> Validate name on card
                if(validateSet($nameOnCard)){
                    // Name on card is set -> Set continuation
                    $dataValid = true;
                } else{
                    // Name on card not set
                    endProcess(false, "Name on card not set");
                }
            } else{
                // Amount not numeric -> End Process
                endProcess(false, "Amount not numeric");
            }
        } else{
            // Amount is not set -> End Process
            endProcess(false, "Amount not set");
        }
    } else{
        // Token not set -> End Process
        endProcess(false, "Token not set");
    }
}

// ----------------------------------------------------------------
// -----------------------Authorize Worldpay-----------------------
// ----------------------------------------------------------------
if($dataValid !== true){
    // Error in validation
    endProcess(false, "Error in validation");
} else{
    // Data is valid -> Start Worldpay authorization
    $worldpayObject->createTransaction($topupAmount, $nameOnCard, $token);
    $authorizeResult = $worldpayObject->authorizeWorldpay();

    // Check authorize result
    if(array_key_exists("SUCCESS", $authorizeResult) && !empty($authorizeResult['SUCCESS']) && ($authorizeResult['SUCCESS'] === true)){
        // Worldpay authorize success
        $worldpayOrderCode = $authorizeResult['ORDER_CODE'];

        // Set continuation
        $worldpayAuthorized = true;
    } else{
        // Worldpay failed to authorize;
        $worldpayAuthorizeResponse = $authorizeResult['RESPONSE'];
        // End the process
        endProcess(false, $authorizeResult);
    }
}

// ----------------------------------------------------------------
// ------------------------Capture Worldpay------------------------
// ----------------------------------------------------------------
if($worldpayAuthorized !== true){
    // Error in validation
    endProcess(false, "Error Worldpay Authorization");
} else{
    // Worldpay Authorized -> Capture Worldpay
    $captureResult = $worldpayObject->captureWorldpay();

    // Check Worldpay Was Captured
    if(array_key_exists("SUCCESS", $captureResult) && !empty($captureResult['SUCCESS']) && ($captureResult['SUCCESS'] === true)){
        $worldpayCaptured = true;
    } else{
        // Loyalty failed to credit card
        $worldpayCaptureResponse = $captureResult['MESSAGE'];
        // End the process
        endProcess(false, $worldpayCaptureResponse);
    }
}

// ----------------------------------------------------------------
// --------------------------Final Check---------------------------
// ----------------------------------------------------------------
if($worldpayCaptured !== true){
    // Error in capture
    endProcess(false, "Error Worldpay Capture");
} else{
    // No errors
    endProcess(true, "Payment success");
}
<?php

/**
 * ----------------------------------------------------------------
 * Worldpay Model
 * ----------------------------------------------------------------
 * @author - Jorden Powley <jorden.powley.webdev@gmail.com>
 * @requires - Worldpay Service Key -> In Config
 *           - Worldpay PGP Library
 */

require_once('../lib/worldpay.php');

class WorldpayModel{
    // Class variables
    protected $worldpay;
    protected $orderDescription;
    protected $amount;
    protected $nameOnCard;
    protected $token;
    protected $worldpayOrderCode;

    /**
     * __construct()
     * @desc - Constructs the Worldpay object
     */
    public function __construct(){
        // Get configuration
        $config;
        if(file_exists('../config.json')){
            $config = file_get_contents('../config.json');
        } else{
            $config = file_get_contents('../../config.json');
        }
        $config = json_decode($config, true);

        $this->worldpay = new Worldpay($config['WORLDPAY']['SERVICE_KEY']);
        $this->orderDescription = $config['WORLDPAY']['ORDER_DESCRIPTION'];
    }

    /**
     * createTransaction()
     * @desc - Creates the Worldpay transaction object
     * @param {int} $amount             - payment amount
     * @param {String} $nameOnCard      - Name on card
     * @param {String} $token           - Token from Worldpay form
     */
    public function createTransaction($amount, $nameOnCard, $token){
        $this->amount = $amount;
        $this->nameOnCard = $nameOnCard;
        $this->token = $token;
    }

    /**
     * authorizeWorldpay()
     * @desc - Authorizes a payment with Worldpay
     * @return {Object[]}
     */
    public function authorizeWorldpay(){
        // Generate datetime for logging
        $logDate = date("Y-m-d H:i:s");

        // Check amount has been set
        if(isset($this->amount) && !empty($this->amount)){
            // Check nameOnCard has been set
            if(isset($this->nameOnCard) && !empty($this->nameOnCard)){
                // Check token has been set
                if(isset($this->token) && !empty($this->token)){
                    try{
                        // Setup and execute Worldpay Order
                        $response = $this->worldpay->createOrder(array(
                            'token' => $this->token,
                            'amount' => $this->amount,
                            'currencyCode' => 'GBP',
                            'billingAddress' => array(
                            'address1'=>'',
                            'address2' => '',
                            'address3'=> '',
                            'postalCode' => '',
                            'city' => '',
                            'state' => '',
                            'countryCode'=> '',
                        ),
                            'name' => $this->nameOnCard,
                            'orderDescription' => $this->orderDescription,
                            'customerOrderCode' => 'N/A',
                            'authoriseOnly' => true
                        ));
                        
                        // Check that Worldpay is authorized
                        if($response['paymentStatus'] === "AUTHORIZED"){
                            // GET order code from response
                            $orderCode = $response['orderCode'];
                            $this->worldpayOrderCode = $orderCode;
            
                            // Return status
                            return array("SUCCESS" => true,
                                         "ORDER_CODE" => $orderCode);
                        } else{
                            // Encode response
                            $code = (array_key_exists('customCode', $response) && isset($response['customCode'])) ? $response['customCode'] : "NA" ;
                            $response = base64_encode(json_encode($response));
            
                            // Return status
                            return array("SUCCESS" => false,
                                         "CODE" => $code, 
                                         "FILE" => "model/WorldpayModel.php", 
                                         "METHOD" => "authorizeWorldpay", 
                                         "RESPONSE" => $response,
                                         "DATETIME" => $logDate);
                        }
                    } catch(WorldpayException $e){
                        // A Worldpay Exception occurred
                        return array("SUCCESS" => false,
                                     "CODE" => "NA",
                                     "FILE" => "model/WorldpayModel.php", 
                                     "METHOD" => "authorizeWorldpay", 
                                     "MESSAGE" => "Worldpay Exception: " . $e->getMessage(),
                                     "DATETIME" => $logDate);
                    } catch(Exception $e){
                        // A non Worlpay exception occurred
                        return array("SUCCESS" => false,
                                     "CODE" => "NA", 
                                     "FILE" => "model/WorldpayModel.php", 
                                     "METHOD" => "authorizeWorldpay", 
                                     "MESSAGE" => "Non-Worldpay Exception: " . $e->getMessage(),
                                     "DATETIME" => $logDate);
                    }
                } else{
                    // Token is not set -> Return error
                    return array("SUCCESS" => false,
                                "CODE" => "NA",
                                "FILE" => "model/WorldpayModel.php", 
                                "METHOD" => "authorizeWorldpay", 
                                "MESSAGE" => "Worldpay -> Token is not set",
                                "DATETIME" => $logDate);
                }
            } else{
                // name on card is not set -> Return error
                return array("SUCCESS" => false,
                            "CODE" => "NA",
                            "FILE" => "model/WorldpayModel.php", 
                            "METHOD" => "authorizeWorldpay", 
                            "MESSAGE" => "Worldpay -> name on card is not set");
            }
        } else{
            // Amount is not set -> Return error
            return array("SUCCESS" => false,
                         "CODE" => "NA",
                         "FILE" => "model/WorldpayModel.php", 
                         "METHOD" => "authorizeWorldpay", 
                         "MESSAGE" => "Worldpay -> Amount is not set",
                         "DATETIME" => $logDate);
        }
    }

    /**
     * captureWorldpay()
     * @desc - Captures a prior authorized transaction
     * @return {Object[]}
     */
    public function captureWorldpay(){
        // Generate datetime for logging
        $logDate = date("Y-m-d H:i:s");
        
        // Check that worldpayOrderCode is set
        if(isset($this->worldpayOrderCode) && !empty($this->worldpayOrderCode)){
            try{
                $this->worldpay->captureAuthorisedOrder($this->worldpayOrderCode);

                // IF no exception is thrown -> SUCCESS
                return array("SUCCESS" => true);
            } catch(WorldpayException $e){
                // A Worldpay Exception occurred
                return array("SUCCESS" => false,
                             "CODE" => "NA",
                             "FILE" => "model/WorldpayModel.php", 
                             "METHOD" => "captureWorldpay", 
                             "MESSAGE" => "Worldpay Exception: " . $e->getMessage(),
                             "DATETIME" => $logDate);
            } catch(Exception $e){
                // Non Worldpay exception has occurred
                return array("SUCCESS" => false,
                             "CODE" => "NA",
                             "FILE" => "model/captureWorldpay.php", 
                             "METHOD" => "captureWorldPay", 
                             "MESSAGE" => "Non-Worldpay Exception: " . $e->getMessage(),
                             "DATETIME" => $logDate);
            }
        } else{
            // Worldpay order code is not set
            return array("SUCCESS" => false,
                         "CODE" => "NA",
                         "FILE" => "model/WorldpayModel.php", 
                         "METHOD" => "captureWorldpay", 
                         "MESSAGE" => "Worldpay -> Order code is not set",
                         "DATETIME" => $logDate);
        }
    }
}
# Worldpay Gateway
An easy to use codeset for speedy integration.

## Setup
Requires a config.json file to be created. Use the config-example.json to help.
Will need an active Worldpay Online account. See here:
[Worldpay Online](https://online.worldpay.com/)
The Worldpay account will require authorizations to be enabled to ensure that the auth method works correctly.

## Required Folders
1. lib*
2. models*

## Example Usage
The main Worldpay payment form is located in index.php. This is called through javascript.
The form styling has to be done on the Worldpay website.
The script within index.php should guide you on what needs to be done for setup.
This script needs a Worldpay client key which I call in using PHP for security reason.
This also makes it easy to change Worldpay credentials when using GIT

An example of a process structure is provided and is functional.
The Worldpay Model is heavily commented and will hopefully be fairly self explanatory, 
however here are the three methods needed (following the auth-capture process structure):

- `$worldpayObject->createTransaction($topupAmount, $nameOnCard, $token);`
- `$worldpayObject->authorizeWorldpay();`
- `$captureResult = $worldpayObject->captureWorldpay();`
